#!/bin/bash
# vim: set noexpandtab:
#
# Copyright 2014-2021 Holger Levsen <holger@layer-acht.org>
#           ©    2018 Mattia Rizzolo <mattia@debian.org>
#
# released under the GPLv2
#
# deployment helper for jenkins.debian.net and build nodes
# (misses freebsd node)

# disclaimers:
#   this script grew over the years…
#   the code is horrible and was always meant to be a quick local hack and never to be published
#   use at your own risk. it might eat your cats. you have been warned.
#   the HOSTS lists below is yet another code duplication…

help() {
cat <<- EOF
accepted params:
 ./deploy_jdn all		- deploy on all nodes (and handle 398 days hosts properly)
 ./deploy_jdn \$host		- deploy on \$host and jenkins (and handle 398 days hosts properly)
 ./deploy_jdn all \$foo		- run "\$foo" on all nodes (and handle 398 days hosts properly)
 ./deploy_jdn 			- deploy on jenkins only
 ./deploy_jdn jenkins		- deploy on jenkins only
 ./deploy_jdn jenkins ionos10	- deploy on jenkins and ionos10
 ./deploy_jdn jenkins o167	- deploy on jenkins and osuosl167
 ./deploy_jdn jenkins c9	- deploy on jenkins and codethink9
 ./deploy_jdn jenkins 10	- deploy on jenkins and ionos10
 ./deploy_jdn jenkins 5 6	- deploy on jenkins and ionos5 and ionos6
 ./deploy_jdn jenkins amd64	- deploy on jenkins and all amd64 nodes
 ./deploy_jdn only ionos10	- deploy on ionos10
 ./deploy_jdn only o167		- deploy on osuosl167
 ./deploy_jdn only c9		- deploy on codethink9
 ./deploy_jdn only i10		- deploy on ionos10
 ./deploy_jdn only 5 6		- deploy on ionos5 and ionos6
 ./deploy_jdn only amd64	- deploy on all amd64 only
 ./deploy_jdn upgrade|u		- run "apt-get update && upgrade && clean" everywhere
 ./deploy_jdn upgradey|uy	- run "apt-get upgrade -y" everywhere
 ./deploy_jdn rmstamp|rm	- delete stamp files everywhere

Rebooting all nodes, except jenkins, is easily done like this:
 $ parallel -j 8 -i sh -c 'ssh {} sudo reboot' -- \$(./nodes/list_nodes | grep -v jenkins)

EOF
}


START=$(date +'%s')
GIT_REPO="https://salsa.debian.org/qa/jenkins.debian.net.git"
mapfile -t HOSTS < <(nodes/list_nodes | grep -v jenkins.debian.net)
HOSTS+=(root@jenkins.debian.net)

ALL_HOSTS=("${HOSTS[@]}")

node_in_the_future () {
	case "$1" in
		ionos5-amd64*|ionos6-i386*|ionos15-amd64*|ionos16-i386*) true ;;
		codethink9*|codethink11*|codethink13*|codethink15*) true ;;
		osuosl170*|osuosl172*) true ;;
		*) false ;;
	esac
}

show_fixmes() {
	#
	# There's always some work left...
	#	echo FIXME is ignored so check-jobs scripts can output templates requiring manual work
	#
	BASEDIR="$(dirname "$(readlink -e $0)")"
	TMPFILE=$(mktemp)
	rgrep FI[X]ME $BASEDIR/* | grep -v $BASEDIR/TODO | grep -v echo > $TMPFILE || true
	if [ -s $TMPFILE ] ; then
		echo
		echo Existing FIXME statements:
		echo
		cat $TMPFILE | sed "s#$PWD#...#g"
		echo
	fi
	rm -f $TMPFILE
}

echo
echo -n "$(date) - "
reset_clock=true
if [ "$1" = "fixme" ] ; then
	show_fixmes
	exit 0
elif [ "$1" = "all" ] ; then
	echo -n "Running j.d.n.git updates on ${HOSTS[@]} now"
	# reset_clock can be false as update_jdn.sh sets the time
	reset_clock=false
	shift
	if [ ! -z "$1" ] ; then
		real_command="$@"
		echo -n "Running '$real_command' on ${HOSTS[@]} now."
		real_command="$@ && echo '__reallyreally=ok__'"
	fi
elif [ "$1" = "upgrade" ] || [ "$1" = "u" ] ; then
	real_command="export LANG=C && sudo apt-get update && sudo apt-get upgrade && sudo apt-get dist-upgrade && sudo apt-get clean"
	shift
elif [ "$1" = "upgradey" ] || [ "$1" = "uy" ] ; then
	real_command="export LANG=C && sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y dist-upgrade && sudo apt-get clean"
	shift
elif [ "$1" = "rmstamp" ] || [ "$1" = "rm" ] ; then
	real_command="sudo rm -f /var/log/jenkins/*stamp && echo '__reallyreally=ok__'"
	reset_clock=false
	shift
elif [ "$1" = "check" ] ; then
	real_command="pgrep -l -a -f _build.sh"
	reset_clock=false
	shift
elif [ "$1" = "" ] ; then
	HOSTS=(root@jenkins.debian.net)
	echo -n "Running j.d.n.git updates on ${HOSTS[@]} now"
elif [ "$1" = "jenkins" ] || [ "$1" = "only" ] ; then
	if [ "$1" = "jenkins" ] ; then
		HOSTS=(root@jenkins.debian.net)
	else
		HOSTS=()
	fi
	shift
	for i in "$@" ; do
		case "$i" in
			1|i1|ionos1)	HOSTS+=(ionos1-amd64.debian.net) ;;
			2|i2|ionos2)	HOSTS+=(ionos2-i386.debian.net) ;;
			3|i3|ionos3)	HOSTS+=(ionos3-i386.debian.net) ;;
			5|i5|ionos5)	HOSTS+=(ionos5-amd64.debian.net) ;;
			6|i6|ionos6)	HOSTS+=(ionos6-i386.debian.net) ;;
			7|i7|ionos7)	HOSTS+=(ionos7-amd64.debian.net) ;;
			9|i9|ionos9)	HOSTS+=(ionos9-amd64.debian.net) ;;
			10|i10|ionos10)	HOSTS+=(ionos10-amd64.debian.net) ;;
			11|i11|ionos11)	HOSTS+=(ionos11-amd64.debian.net) ;;
			12|i12|ionos12)	HOSTS+=(ionos12-i386.debian.net) ;;
			15|i15|ionos15)	HOSTS+=(ionos15-amd64.debian.net) ;;
			16|i16|ionos16)	HOSTS+=(ionos16-i386.debian.net) ;;
			o167)		HOSTS+=(osuosl167-amd64.debian.net) ;;
			o168)		HOSTS+=(osuosl168-amd64.debian.net) ;;
			o170)		HOSTS+=(osuosl170-amd64.debian.net) ;;
			o171)		HOSTS+=(osuosl171-amd64.debian.net) ;;
			o172)		HOSTS+=(osuosl172-amd64.debian.net) ;;
			o173)		HOSTS+=(osuosl173-amd64.debian.net) ;;
			o174)		HOSTS+=(osuosl174-amd64.debian.net) ;;
			o184)		HOSTS+=(osuosl184-amd64.debian.net) ;;
			c9|cs9|ct9)	HOSTS+=(codethink9-arm64.debian.net) ;;
			c10|cs10|ct10)	HOSTS+=(codethink10-arm64.debian.net) ;;
			c11|cs11|ct11)	HOSTS+=(codethink11-arm64.debian.net) ;;
			c12|cs12|ct12)	HOSTS+=(codethink12-arm64.debian.net) ;;
			c13|cs13|ct13)	HOSTS+=(codethink13-arm64.debian.net) ;;
			c14|cs14|ct14)	HOSTS+=(codethink14-arm64.debian.net) ;;
			c15|cs15|ct15)	HOSTS+=(codethink15-arm64.debian.net) ;;
			c16|cs16|ct16)	HOSTS+=(codethink16-arm64.debian.net) ;;
			armhf|amd64|i386|arm64)	 HOSTS+=($(echo "${ALL_HOSTS[@]}" | sed -e 's# #\n#g' | grep "$i")) ;;
			*) 	if ping -c 1 "$i" ; then HOSTS+=("$i") ; fi ;;
		esac
	done
	echo -n "Running j.d.n.git updates on ${HOSTS[@]} now"
else
	echo "I dont understand what to do, please try again."
	help
	exit 1
fi

BG=""

get_arch_color() {
	case "$1" in
		*amd64*)		BG=lightgreen ;;
		*i386*)			BG=lightblue ;;
		*arm64*)		BG=orange ;;
		*armhf*)		BG=lightyellow ;;
		*jenkins.debian.*)	BG=yellow ;;
		*)				BG=white ;;
	esac
}

LOG="$(mktemp -u)"
# reverse the array
STSOH=()
for i in "${HOSTS[@]}" ; do
	STSOH=("$i" "${STSOH[@]}")
done
HOSTS=("${STSOH[@]}")

for i in "${HOSTS[@]}" ; do
	if [ -z "$real_command" ]; then
		if node_in_the_future "$i"; then
			GITOPTS="-c http.sslVerify=false"
		fi
		# real command, for running manually: cd ~jenkins-adm/jenkins.debian.net/ ; sudo -H -u jenkins-adm git pull ; ./update_jdn.sh
		read -r -d '' remote_command <<-EOF
			set -e
			export LANG=C
			cd ~jenkins-adm
			if [ ! -d jenkins.debian.net ]; then
				[ -x /usr/bin/git ] || sudo apt-get install -y git
				sudo -H -u jenkins-adm git ${GITOPTS:-} clone $GIT_REPO
				cd jenkins.debian.net
			else
				cd jenkins.debian.net
				sudo -H -u jenkins-adm git config pull.ff only
				sudo -H -u jenkins-adm git ${GITOPTS:-} pull $GIT_REPO
			fi
			./update_jdn.sh 2>&1 | sudo tee -a /var/log/jenkins/update_jdn.log
		EOF
	else
		remote_command="$real_command"
	fi

	echo -n "."
	if $reset_clock ; then
		if node_in_the_future "$i" ; then
			#  set correct future date
			case "$i" in
				osuosl*)	NTP_SERVER=time.osuosl.org ;;
				*)		NTP_SERVER=de.pool.ntp.org ;;
			esac
			remote_command="sudo ntpdate -b $NTP_SERVER && $remote_command && sudo date --set=\"+398 days +6 hours + 23 minutes\" && echo '__$(echo $i|cut -d '.' -f1)=ok__'"
		fi
	fi
	get_arch_color $i
	xterm -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "ssh -o 'BatchMode = yes' -t $i '$remote_command' | tee $LOG.$i ; sleep 2 ; touch $LOG.$i.done" &
	unset GITOPTS
done
sleep 3
COUNTER=0
for i in "${HOSTS[@]}" ; do
	while ! test -f $LOG.$i.done ; do
		let COUNTER+=1
		sleep 1
		echo -n "."
		if [ $COUNTER -eq 42 ] ; then
			echo 
			echo -n "$LOG.$i.done still doesnt exist, how strange…"
			COUNTER=0
			continue
		fi
	done
done
echo

JENKINS_OFFLINE_LIST="$(dirname $0)/jenkins-home/offline_nodes"
echo
echo JENKINS_OFFLINE_LIST=$JENKINS_OFFLINE_LIST
echo
OFFLINE=""
PROBLEMS=""
for i in "${HOSTS[@]}" ; do
	HNAME1=$(echo $i | cut -d "@" -f2 | cut -d "." -f1|cut -d "-" -f1)	# IONOS nodes (h01ger)
	HNAME2=$(echo $i | cut -d "@" -f2 | cut -d "." -f1)			# non -armhf ones (vagrant)
	echo "Checking for problems on $i $HNAME1 $HNAME2..."
	TAIL=$(tail -1 $LOG.$i 2>/dev/null)
	if [ "$i" = "root@jenkins.debian.net" ] ; then
		if ! ( [[ "$TAIL" =~ "__$HNAME1=ok__" ]] || [[ "$TAIL" =~ "__$HNAME2=ok__" ]] || [[ "$TAIL" =~ "__reallyreally=ok__" ]] || [[ "$TAIL" =~ "0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded." ]] ) ; then
			echo "Problems on $i:"
		fi
		cat $LOG.$i
		rm $LOG.$i $LOG.$i.done > /dev/null
	elif [[ "$TAIL" =~ "__$HNAME1=ok__" ]] || [[ "$TAIL" =~ "__$HNAME2=ok__" ]] || [[ "$TAIL" =~ "__reallyreally=ok__" ]] || [[ "$TAIL" =~ "0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded." ]] || [ ! -f $LOG.$i ] ; then
		rm $LOG.$i $LOG.$i.done > /dev/null
	else
		if [ -f "$JENKINS_OFFLINE_LIST" ]; then
			if grep -q "$i" "$JENKINS_OFFLINE_LIST"; then
				# node was known to be offline
				if [ -z "$OFFLINE" ] ; then
					OFFLINE=" $i ($LOG.$i)"
				else
					OFFLINE=" $i ($LOG.$i)\n$OFFLINE"
				fi
			else
				# unexpected problem
				if [ -z "$PROBLEMS" ] ; then
					PROBLEMS=" $i"
				else
					PROBLEMS=" $i\n$PROBLEMS"
				fi
				get_arch_color $i
				xterm -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "less +G $LOG.$i ; rm $LOG.$i $LOG.$i.done" &
			fi
		fi
	fi
done
echo
echo

echo "$(echo "${HOSTS[@]}" | sed -s "s# #\n#g" | wc -l) hosts updated."
echo
if [ ! -z "$PROBLEMS" ] ; then
	echo "Problems on:"
	echo -e "$PROBLEMS"
	echo
fi
if [ ! -z "$OFFLINE" ] ; then
	echo "Offline nodes with unsurprising problems encountered (not showing the failed deploy log):"
	echo -e "$OFFLINE"
	echo
fi
show_fixmes
END=$(date +'%s')
DURATION=$(( $END - $START ))
HOUR=$(echo "$DURATION/3600"|bc)
MIN=$(echo "($DURATION-$HOUR*3600)/60"|bc)
SEC=$(echo "$DURATION-$HOUR*3600-$MIN*60"|bc)
echo "$(date) - total duration: ${HOUR}h ${MIN}m ${SEC}s."
echo

