#!/bin/bash

# Copyright 2015-2021 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2

DEBUG=true
. /srv/jenkins/bin/common-functions.sh
common_init "$@"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

# that's all
rsync_remote_results() {
	PROJECT=$1
	NODE=$2
	echo "$(date -u) - Starting to rsync results for '$PROJECT'."
	local RESULTS=$(mktemp --tmpdir=$BASE/.. -d reproducible-rsync-${BUILD_ID}-XXXXXXXXX)
	# copy the new results from build node to webserver node
	if $DEBUG ; then
		RSYNCCMD="rsync -r -v -e \"ssh -o 'Batchmode = yes'\" $NODE:$BASE/$PROJECT/ $RESULTS 2>/dev/null"
	else
		RSYNCCMD="rsync -r -v -e \"ssh -o 'Batchmode = yes'\" $NODE:$BASE/$PROJECT/ $RESULTS"
	fi
	if eval $RSYNCCMD ; then
		chmod 775 $RESULTS
		# move old results out of the way
		if [ -d $BASE/$PROJECT ] ; then
			mv $BASE/$PROJECT ${RESULTS}.tmp
			# preserve images and css
			for OBJECT in $(find ${RESULTS}.tmp -name "*css" -o -name "*png" -o -name "*jpg") ; do
				cp -v $OBJECT $RESULTS/
			done
			# delete the old results
			rm ${RESULTS}.tmp -r
		fi
		# make the new results visible
		mv $RESULTS $BASE/$PROJECT
		echo "$(date -u) - $REPRODUCIBLE_URL/$PROJECT has been updated."
	else
		echo "$(date -u) - no new results for '$PROJECT' found."
	fi
}

# main
echo "$(date -u) - Starting to rsync results."
rsync_remote_results coreboot 		osuosl171-amd64.debian.net
rsync_remote_results netbsd		osuosl171-amd64.debian.net
echo "$(date -u) - the end."
